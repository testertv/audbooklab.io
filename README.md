<a href=""><img src="https://gitlab.com/testertv/audbook.gitlab.io/-/raw/master/img/top.jpg?raw=true" alt="test-pattern-152459-1280" border="0"></a>



<h1 style="text-align: center;"><span style="color: #3366ff;"><strong>Audbook Creator</strong></span></h1>



Audbook Creator is the simple program that will help you transform any text into an audio file/book. To create audio files, text is sent to the Yandex search engine and converted to an audio file. The search engine has a limit on the length of text of 1000 letters. So my program splits your text into fragments of <1000 letters, and after downloading all fragments you can press the button "Merge all audios" to merge all audio files into one. The program supports 4 languages:


- English

- Russian

- Ukraine

- Turky



<h3 style="text-align: center;"><span style="color: #3366ff;"><strong>Screenshots:</strong></span></h3>



<a href=""><img src="https://gitlab.com/testertv/audbook.gitlab.io/-/raw/master/img/screen.jpg?raw=true" alt="test-pattern-152459-1280" border="0"></a>



<h3 style="text-align: center;"><span style="color: #3366ff;"><strong>Download last version for Windows (free, portable and open source):</strong></span></h3>



<h3><span style="text-decoration: underline;"><strong>https://gitlab.com/testertv/audbook.gitlab.io/-/raw/master/soft/Audbook.Creator_v.2020.12.08.zip</strong></span></h3>



Audio sample: <h3><span style="text-decoration: underline;"><strong>https://gitlab.com/testertv/audbook.gitlab.io/-/raw/master/sample.mp3</strong></span></h3>


<h3 style="text-align: center;"><span style="color: #3366ff;"><strong>Installation:</strong></span></h3>



Extract archive and put the program "ffmpeg.exe" in the "ffmpeg" folder! 
(https://github.com/BtbN/FFmpeg-Builds/releases)


<h4 style="text-align: center;"><span style="color: #3366ff;"><strong>Attention: the path where the program is located must not contain "spaces" or "non-English letters".</strong></span></h4>


P.S.: I am not a programmer, so do not judge strictly my code! The main thing it performs its function :)

my old repository was on GitHub, but because of the change of the authentication policy I left the site and came here.
